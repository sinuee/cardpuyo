#include "..\Headers\puyo-puyo.h"
#include <conio.h>


int _tmain(int argc, _TCHAR* argv[]){

	Tablero TableroJuego;
	bool bContinuar = true;
	bool Start = true;
	char car;
	std::string Buffer;
	bool op = true;
	
	while(bContinuar){
		TableroJuego.Jugando(Start);
		Start = false;
		
		if (!TableroJuego.bJugando){
			TableroJuego.imprimePantalla();
			car=getch();
			Buffer = car;
			while(op){
				if (Buffer=="y" || Buffer=="Y"){
					bContinuar = true;
					Start = true;
					op = false;
				}else if(Buffer=="n" || Buffer=="N"){
					bContinuar = false;
					op = false;
				}
				else{
					op = true;
				}
			};
		}

		Sleep(500);
	};
}
#include "..\Headers\Tablero.h"

Tablero::Tablero(){
	Posicion centro;
	Score = 0;
	int x,y;
	for(y=0;y<iRenglones;y++){
		for (x=0;x<iPilas;x++){		
			centro.x = (3*x)+1;
			centro.y = (3*y)+1;
			puyo Puyo(centro);
			Puyos.push_back(Puyo);
		}
	}
	Pausa = false;
	iniciarPantalla();
}
Tablero::~Tablero(){
}

void Tablero::iniciarPantalla(){

	// Inicializar Handlers:
    wHnd = GetStdHandle(STD_OUTPUT_HANDLE);
    rHnd = GetStdHandle(STD_INPUT_HANDLE);
	
	SetConsoleTitle(TEXT("Puyo Puyo"));
	
	// tamaño de consola:
    SMALL_RECT windowSize ;//= {0, 0, iWidth, iHeight};
	windowSize.Top = 0;
	windowSize.Left=0;
	windowSize.Right=iWidth-1;
	windowSize.Bottom = iHeight-1;
    
    // Cambiamos el tamaño de la consola:
    SetConsoleWindowInfo(wHnd, TRUE, &windowSize);
	
	COORD bufferSize = {iWidth,iHeight};

    // Change the internal buffer size:
    SetConsoleScreenBufferSize(wHnd, bufferSize);

}
void Tablero::imprimePantalla(){
	int x;
  for (int y = 0; y < iHeight; ++y) {
		for (x = 0; x < iColumnas; ++x) {
            Pantalla[x + iWidth * y].Char.AsciiChar = ' ';
            Pantalla[x + iWidth * y].Attributes = BACKGROUND_INTENSITY;
        }
		for (int k = x; k < iWidth; ++k) {
            Pantalla[k + iWidth * y].Char.AsciiChar = ' ';
            Pantalla[k + iWidth * y].Attributes = FOREGROUND_GREEN | FOREGROUND_INTENSITY;
        }
    }
  for (int i = 0;i<Puyos.size();i++)
	  Puyos[i].imprimePuyo(wHnd, Pantalla);

  PantallaScore();
  if (!bJugando){
	  ImprimeGameOver();
  }

  COORD charBufSize = {iWidth,iHeight};
  COORD characterPos = {0,0};
  SMALL_RECT writeArea = {0,0,iWidth-1,iHeight-1}; 
  
  WriteConsoleOutputA(wHnd, Pantalla, charBufSize, characterPos, &writeArea);

}
void Tablero::CapturarEventos(){
		
    DWORD numEvents = 0;
    DWORD numEventsRead = 0;
	DWORD i = 0;

	Rotar = false;
	MvDer = false;
	MvIzq = false;
	MvAbJ = false;
	
	GetNumberOfConsoleInputEvents(rHnd, &numEvents);
	
	if (numEvents != 0) {
        INPUT_RECORD *eventBuffer = new INPUT_RECORD[numEvents];
        ReadConsoleInput(rHnd, eventBuffer, numEvents, &numEventsRead);
        if (eventBuffer[i].EventType == KEY_EVENT) {
            if (eventBuffer[i].Event.KeyEvent.wVirtualKeyCode==VK_ESCAPE) {
				bJugando = false;
				exit(0);
            }
			if (eventBuffer[i].Event.KeyEvent.wVirtualKeyCode==VK_RIGHT){
				MvDer = true;
			}
			if (eventBuffer[i].Event.KeyEvent.wVirtualKeyCode==VK_LEFT){
				MvIzq = true;
			}
			if (eventBuffer[i].Event.KeyEvent.wVirtualKeyCode==VK_DOWN){
				MvAbJ = true;
			}
			if (eventBuffer[i].Event.KeyEvent.wVirtualKeyCode==VK_UP){
				Rotar = true;
			}
			if (eventBuffer[i].Event.KeyEvent.wVirtualKeyCode==VK_RETURN){
				Pausa = !Pausa;
			}
		}//If key events
        delete[] eventBuffer;
	}//if events
}
void Tablero::Jugando(bool Start){
		
	MvDer = false;
	MvIzq = false;
	Rotar = false;
	MvAbJ = false;

	if (!Pausa){
		if (Start){
			LimpiarTablero();
			NuevoPar();
			Puyos[iActual[0]].Cayendo = true;
			Puyos[iActual[1]].Cayendo = true;
		}else if (Puyos[iActual[0]].Cayendo || Puyos[iActual[1]].Cayendo){
			CapturarEventos();
			if (MvDer){
				if (Puyos[iActual[0]].obtenerXv() < Puyos[iActual[1]].obtenerXv()){
					MoverDerecha(iActual[1],1);
					MoverDerecha(iActual[0],0);
				}else{
					MoverDerecha(iActual[0],0);
					MoverDerecha(iActual[1],1);					
				}
			}else
			if (MvIzq){
				if (Puyos[iActual[0]].obtenerXv() > Puyos[iActual[1]].obtenerXv()){
					MoverIzquierda (iActual[1],1);
					MoverIzquierda(iActual[0],0);
				}else{
					MoverIzquierda(iActual[0],0);
					MoverIzquierda(iActual[1],1);					
				}
			}else
			if(Rotar){
				fRotar();
			}else
			if(MvAbJ){
				if (Puyos[iActual[0]].obtenerYv() < Puyos[iActual[1]].obtenerYv()){
					MoverAbajo(iActual[1]);
					MoverAbajo(iActual[0]);
				}else{
					MoverAbajo(iActual[0]);
					MoverAbajo(iActual[1]);					
				}
			}else{
				if (Mover()){
					/*Puyos[iActual[0]].Cayendo = true;
					Puyos[iActual[1]].Cayendo = true;*/
				}else{
					Puyos[iActual[0]].Cayendo = false;
					Puyos[iActual[1]].Cayendo = false;
					HacerRenza(iActual[0]);
					HacerRenza(iActual[1]);
					Descender();
				}
			}
		}else{
			NuevoPar();
			Puyos[iActual[0]].Cayendo = true;
			Puyos[iActual[1]].Cayendo = true;
		}
		HacerRenza(iActual[0]);
		HacerRenza(iActual[1]);
		imprimePantalla();
	}else{
		CapturarEventos();
	}
}
void Tablero::LimpiarTablero(){
	int i;
	for (i=0;i<Puyos.size();i++)
		Puyos[i].nuevoPuyo(Puyos[i].DarCentro());
}
void Tablero::NuevoPar(){
	int aux;
	if((Puyos[9].estado == ACTIVO)  || (Puyos[10].estado == ACTIVO)){
		bJugando = false;
	}else{
		bJugando = true;
		srand(time(NULL));
		Puyos[3].Pivote=true;
		Puyos[3].estado = ACTIVO;
		Puyos[3].Cayendo = true;
		aux = rand() % 4 + 3;
		switch (aux){
			case 3: 
				Puyos[3].tipo = PICA;
				break;
			case 4:
				Puyos[3].tipo = TREBOL;
				break;
			case 5:
				Puyos[3].tipo = AS;
				break;
			case 6:
				Puyos[3].tipo = DIAMANTE;
				break;
		};
		iActual[0] = 3;
		//srand(time(NULL)+333); 
		Puyos[4].Pivote=false;
		Puyos[3].Cayendo = true;
		Puyos[4].estado = ACTIVO;
		aux = rand() % 4 + 3;
		switch (aux){
			case 3: 
				Puyos[4].tipo = PICA;
				break;
			case 4:
				Puyos[4].tipo = TREBOL;
				break;
			case 5:
				Puyos[4].tipo = AS;
				break;
			case 6:
				Puyos[4].tipo = DIAMANTE;
				break;
		};
		iActual[1] = 4;
		
	}
}
bool Tablero::Mover(){
	bool Movi=false;
	int X,Y;
	int MoverPrimero, MoverDespues;	
	if (Puyos[iActual[0]].obtenerYv() <Puyos[iActual[1]].obtenerYv()){
		MoverPrimero = 1;
		MoverDespues = 0;
	}else{
		MoverPrimero = 0;
		MoverDespues = 1;
	}
	Y = Puyos[iActual[MoverPrimero]].obtenerYv();
	if (ChercarDescenso(iActual[MoverPrimero])){
		if (Y+1<=11){
			Y++;
			X=Puyos[iActual[MoverPrimero]].obtenerXv();
			PasarPuyo(iActual[MoverPrimero],X+(Y*6));
			iActual[MoverPrimero] = X+(Y*6);
			Movi=true;
		}		
	}else{
		if (Y < 11){//it's in the botom
			HacerRenza(iActual[MoverPrimero]);
		}else{
			HacerRenza(iActual[MoverPrimero]);
		}
		Puyos[iActual[MoverPrimero]].Cayendo = false;
	}
	Y = Puyos[iActual[MoverDespues]].obtenerYv();
	if (ChercarDescenso(iActual[MoverDespues])){
		if (Y+1<=11){
			Y++;
			X=Puyos[iActual[MoverDespues]].obtenerXv();
			PasarPuyo(iActual[MoverDespues],X+(Y*6));
			iActual[MoverDespues] = X+(Y*6);
			Movi=true;
		}
	}else{
		if (Y < 11){//it's in the botom
			HacerRenza(iActual[MoverDespues]);
		}else{
			HacerRenza(iActual[MoverDespues]);
		}
		Puyos[iActual[MoverDespues]].Cayendo = false;
	}
	return Movi;
}
void Tablero::PasarPuyo(int Org, int Dst){
	Puyos[Dst].estado = Puyos[Org].estado;
	Puyos[Dst].Simbolo = Puyos[Org].Simbolo;
	Puyos[Dst].tipo = Puyos[Org].tipo;
	Puyos[Dst].Pivote = Puyos[Org].Pivote; 
	Puyos[Dst].Cayendo = Puyos[Org].Cayendo;
	Puyos[Org].estado = INACTIVO;
	Puyos[Org].Cayendo = false;
}
void Tablero::HacerRenza(int Indice){
	std::vector<int> IndicesRenza;
	int X,Y;//lugar del vecinos
	int i = 0;
	int j;
	bool hayVecino=true;
	bool yaEsta = false;
	if (!Puyos[Indice].Cayendo){
		IndicesRenza.push_back(Indice);
		while(hayVecino){
			hayVecino = false;
			//right
			X=Puyos[IndicesRenza[i]].obtenerXv()+1;
			Y=Puyos[IndicesRenza[i]].obtenerYv();
			if (X<6){
				if(Puyos[IndicesRenza[i]].estado == ACTIVO && Puyos[IndicesRenza[i]] == Puyos[X+(Y*6)]){
					for (j=0;j<(int)IndicesRenza.size();j++){
						if (IndicesRenza[j] == X+(Y*6)){
							yaEsta = true;
							break;
						}else{
							yaEsta = false;
						}
					}
					if (!yaEsta){
						IndicesRenza.push_back(X+(Y*6));
						hayVecino = true;
					}
				}
			}
			//left
			X=Puyos[IndicesRenza[i]].obtenerXv()-1;
			Y=Puyos[IndicesRenza[i]].obtenerYv();
			if (X>=0){
				if(Puyos[IndicesRenza[i]].estado == ACTIVO && Puyos[IndicesRenza[i]] == Puyos[X+(Y*6)]){
					for (j=0;j<(int)IndicesRenza.size();j++){
						if (IndicesRenza[j] == X+(Y*6)){
							yaEsta = true;
							break;
						}else{
							yaEsta = false;
						}
					}
					if (!yaEsta){
						IndicesRenza.push_back(X+(Y*6));
						hayVecino = true;
					}
				}
			}
			//down
			X=Puyos[IndicesRenza[i]].obtenerXv();
			Y=Puyos[IndicesRenza[i]].obtenerYv()+1;
			if (Y<12){
				if(Puyos[IndicesRenza[i]].estado == ACTIVO && Puyos[IndicesRenza[i]] == Puyos[X+(Y*6)]){
					for (j=0;j<(int)IndicesRenza.size();j++){
						if (IndicesRenza[j] == X+(Y*6)){
							yaEsta = true;
							break;
						}else{
							yaEsta = false;
						}
					}
					if (!yaEsta){
						IndicesRenza.push_back(X+(Y*6));
						hayVecino = true;
					}
				}
			}
			//up
			X=Puyos[IndicesRenza[i]].obtenerXv();
			Y=Puyos[IndicesRenza[i]].obtenerYv()-1;
			if (Y>=0){
				if(Puyos[IndicesRenza[i]].estado == ACTIVO && Puyos[IndicesRenza[i]] == Puyos[X+(Y*6)]){
					for (j=0;j<(int)IndicesRenza.size();j++){
						if (IndicesRenza[j] == X+(Y*6)){
							yaEsta = true;
							break;
						}else{
							yaEsta = false;
						}
					}
					if (!yaEsta){
						IndicesRenza.push_back(X+(Y*6));
						hayVecino = true;
					}
				}
			}
			i++;
		}
		if (IndicesRenza.size() > 3){
			CalcularScore((int)IndicesRenza.size());
			for(int i=0;i<(int)IndicesRenza.size();i++){
				Puyos[IndicesRenza[i]].estado=INACTIVO;
				Puyos[IndicesRenza[i]].Cayendo = false;
			}
			Descender();
		}
	}//If (!Cayendo)
}

void Tablero::CalcularScore(int iPuyo){
	 Score = Score + ((iPuyo * 10) * (iPuyo - 3));
}
void Tablero::MoverAbajo(int Indice){
	int X,Y,i;
	X=Puyos[Indice].obtenerXv();
	Y=Puyos[Indice].obtenerYv();
	//serch puyos down current puyo
	if (Y < 11){
		for (i=Y;i<11;i++){
			if (Puyos[X+((i+1)*6)].estado == ACTIVO){
				if (Indice !=  X+(i*6)){
					PasarPuyo(Indice,X+(i*6));
				}
				Puyos[Indice].Cayendo = false;
				imprimePantalla();
				return;
			}
		}
	}
	//didn't found an puyo to place up
	if (Puyos[X+(11*6)].estado == INACTIVO){
		if (Indice !=  X+(i*6)){
					PasarPuyo(Indice,X+(i*6));
				}
				Puyos[Indice].Cayendo = false;
				imprimePantalla();
	}
}
void Tablero::MoverDerecha(int Indice, int Act){
	int X,Y;
	X=Puyos[Indice].obtenerXv()+1;
	Y=Puyos[Indice].obtenerYv();
	if (Puyos[Act].Cayendo){
		if (!(X>=6)){//Not MOve when X >=5
			if (Puyos[X+(Y*6)].estado==INACTIVO){//Mover
				PasarPuyo(Indice,X+(Y*6));
				iActual[Act] = X+(Y*6);
			}
		}
	}
}
void Tablero::MoverIzquierda(int Indice,int Act){
	int X,Y;
	X=Puyos[Indice].obtenerXv()-1;
	Y=Puyos[Indice].obtenerYv();
	if (Puyos[Act].Cayendo){
		if (!(X<0)){//Not MOve when X <=0
			if (Puyos[X+(Y*6)].estado==INACTIVO){//Move
				PasarPuyo(Indice,X+(Y*6));
				iActual[Act] = X+(Y*6);
			}
		}
	}
}
void Tablero::fRotar(){
	enum Tipo tipoAux;
	tipoAux = Puyos[iActual[0]].tipo;
	/*Puyos[iActual[0]].tipo = Puyos[iActual[1]].tipo;
	Puyos[iActual[1]].tipo = tipoAux;*/
	int X,Y, Indice;
	//saber donde estoy respecto al pivote.
	if (Puyos[iActual[0]].obtenerYv() > Puyos[iActual[1]].obtenerYv()){//pivot is dwon, rotate to the right
		if (Puyos[iActual[0]].obtenerXv() < 5){
			Y = Puyos[iActual[0]].obtenerYv();
			X = Puyos[iActual[0]].obtenerXv() + 1;
			Indice = X+(Y*6);
			PasarPuyo(iActual[1],Indice);
			iActual[1] = Indice;
		}
	}else if (Puyos[iActual[0]].obtenerYv() < Puyos[iActual[1]].obtenerYv()){//the pivot is up then rotate to the left
		if (Puyos[iActual[0]].obtenerXv() > 0){
			Y = Puyos[iActual[0]].obtenerYv();
			X = Puyos[iActual[0]].obtenerXv() - 1;
			Indice = X+(Y*6);
			PasarPuyo(iActual[1],Indice);
			iActual[1] = Indice;
		}
	}else if (Puyos[iActual[0]].obtenerXv() < Puyos[iActual[1]].obtenerXv()){//the pivot is on the left then move dwon
		if (Puyos[iActual[1]].obtenerYv() < 11){
			Y = Puyos[iActual[0]].obtenerYv() + 1;
			X = Puyos[iActual[0]].obtenerXv();
			Indice = X+(Y*6);
			PasarPuyo(iActual[1],Indice);
			iActual[1] = Indice;
		}
	}else{//pivot on the right
		if (Puyos[iActual[1]].obtenerYv() > 1){
			Y = Puyos[iActual[0]].obtenerYv() - 1;
			X = Puyos[iActual[0]].obtenerXv();
			Indice = X+(Y*6);
			PasarPuyo(iActual[1],Indice);
			iActual[1] = Indice;
		}
	}
}
void Tablero::Descender(){
	int i;
	for (i=iPilas*iRenglones-1;i>0;i--){
		if (Puyos[i].estado == ACTIVO){
			MoverAbajo(i);	
			HacerRenza(i);
		}
	}
}
bool Tablero::ChercarDescenso(int Indice){
	int X,Y;
	X=Puyos[Indice].obtenerXv();
	Y=Puyos[Indice].obtenerYv()+1;
	if (Y>=12){
		return false;
	}
	if (Puyos[X+(Y*6)].estado==INACTIVO){
		return true;
	}
	return false;
}
void Tablero::PantallaScore(){
	int i,j;
	int inicioX = 18;
	int inicioY = 0;
	int FinX = 20;
	int FinY = 36;
	char Marcador[6];
	//Pantalla[260].Char.AsciiChar = 'S';
	//for (i=inicioX;i<FinX;i++)
	i=inicioX;
		for(j=inicioY;j<FinY;j++){
			Pantalla[i+(iWidth*j)].Attributes = FOREGROUND_RED|FOREGROUND_INTENSITY  | BACKGROUND_RED  |BACKGROUND_GREEN | BACKGROUND_INTENSITY;
			Pantalla[i+(iWidth*j)].Char.AsciiChar = 0xBA;
		}
	i = 22;
	j = 3;
Pantalla[i+(iWidth*j)].Attributes = FOREGROUND_GREEN | FOREGROUND_INTENSITY;
Pantalla[i+(iWidth*j)].Char.AsciiChar = 'S';
i=23;
Pantalla[i+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
Pantalla[i+(iWidth*j)].Char.AsciiChar = 'C';
i=24;
Pantalla[i+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
Pantalla[i+(iWidth*j)].Char.AsciiChar = 'O';
i=25;
Pantalla[i+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
Pantalla[i+(iWidth*j)].Char.AsciiChar = 'R';
i=26;
Pantalla[i+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
Pantalla[i+(iWidth*j)].Char.AsciiChar = 'E';

j=5;
i=22;
int k=0;
itoa(Score,Marcador,10);
Pantalla[i+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
if (Score<100000){
	Pantalla[i+(iWidth*j)].Char.AsciiChar = '0';
}else{
	Pantalla[i+(iWidth*j)].Char.AsciiChar = Marcador[k];
	k++;
}


j=5;
i=23;
Pantalla[i+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
if (Score<10000){
	Pantalla[i+(iWidth*j)].Char.AsciiChar = '0';
}else{
	Pantalla[i+(iWidth*j)].Char.AsciiChar = Marcador[k];
	k++;
}

j=5;
i=24;
Pantalla[i+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
if (Score<1000){
	Pantalla[i+(iWidth*j)].Char.AsciiChar = '0';
}else{
	Pantalla[i+(iWidth*j)].Char.AsciiChar = Marcador[k];
	k++;
}

j=5;
i=25;
Pantalla[i+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
if (Score<100){
	Pantalla[i+(iWidth*j)].Char.AsciiChar = '0';
}else{
	Pantalla[i+(iWidth*j)].Char.AsciiChar = Marcador[k];
	k++;
}

j=5;
i=26;
Pantalla[i+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
if (Score<10){
	Pantalla[i+(iWidth*j)].Char.AsciiChar = '0';
}else{
	Pantalla[i+(iWidth*j)].Char.AsciiChar = Marcador[k];
	k++;
}

j=5;
i=27;
Pantalla[i+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
if (Score<1){
	Pantalla[i+(iWidth*j)].Char.AsciiChar = '0';
}else{
	Pantalla[i+(iWidth*j)].Char.AsciiChar = Marcador[k];
	k++;
}

std::string Buffer;
j=7;
i=23;
char car;
Buffer =+ " Mover a la derecha: rigth    "; 
for (k=0;k<30;k++){
	Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
	car= Buffer.c_str()[k];
	Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
}

j=9;
i=23;
Buffer =+ " Mover a la izquierda: left   "; 
for (k=0;k<30;k++){
	Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
	car= Buffer.c_str()[k];
	Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
}

j=11;
i=23;
Buffer =+ " Mover rapido para abajo: down"; 
for (k=0;k<30;k++){
	Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
	car= Buffer.c_str()[k];
	Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
}

j=13;
i=23;
Buffer =+ " Para pausar : Enter          "; 
for (k=0;k<30;k++){
	Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
	car= Buffer.c_str()[k];
	Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
}

j=15;
i=23;
Buffer =+ " Salir: Esc                   "; 
for (k=0;k<30;k++){
	Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
	car= Buffer.c_str()[k];
	Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
}

j=17;
i=23;
Buffer =+ " Para girar : Arriba          "; 
for (k=0;k<30;k++){
	Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_GREEN  | FOREGROUND_INTENSITY;
	car= Buffer.c_str()[k];
	Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
}


}

void Tablero::ImprimeGameOver(){
	int i,j,k;
	std::string Buffer;
	char car;

	j=15;
	i=16;
	k=0;
	Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN ;
	car= 0xC9;
	Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
	for (k=1;k<23;k++){
		Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN ;
		car= 0xCD;
		Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
	}
	Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN ;
	car= 0xBB;
	Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
	
	j=16;
	i=16;
	Buffer = 0xBA;
	Buffer =+ "      Otra Partida?   "; 
	Buffer =+ 0xBA;
	for (k=0;k<23;k++){
		Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN ;
		car= Buffer.c_str()[k];
		Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
	}
	Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN ;
	Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = 0xBA;

	j=17;
	i=16;
	Buffer = 0xBA;
	Buffer =+ "       Y/N            "; 
	Buffer =+ 0xBA;
	for (k=0;k<23;k++){
		Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN ;
		car= Buffer.c_str()[k];
		Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
	}
	Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN ;
	Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = 0xBA;

	j=18;
	i=16;
	k=0;
	Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN ;
	car= 0xC8;
	Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
	for (k=1;k<23;k++){
		Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN ;
		car= 0xCD;
		Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
	}
	Pantalla[(i+k)+(iWidth*j)].Attributes = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN ;
	car= 0xBC;
	Pantalla[(i+k)+(iWidth*j)].Char.AsciiChar = car;
	
}

#include "..\Headers\puyo.h"


void puyo::asignarPos(int _x, int _y){
		Centro.x = _x;
		Centro.y = _y;
	}
int puyo::obtenerCentro(){
		return Centro.x + iWidth * Centro.y;
	}

puyo::puyo(Posicion Centro){
		nuevoPuyo(Centro);
	}
void puyo::nuevoPuyo(Posicion _Centro){
	Centro = _Centro;
	srand(time(NULL));
	int aaaa = time(NULL);
	aaaa = rand() % 4 + 3;
	tipo = DIAMANTE;
	estado = INACTIVO;
}
int puyo::obtenerX(){
	return Centro.x;
}
int puyo::obtenerY(){
	return Centro.y;
}
int puyo::obtenerXv(){
	return (int)(Centro.x-1)/3;
}
int puyo::obtenerYv(){
	return (int)(Centro.y-1)/3;
}
puyo::~puyo(){
}
void puyo::imprimePuyo(HANDLE &wHnd,CHAR_INFO *Pantalla){

	COORD charBufSize = {iWidth,iHeight};
    COORD characterPos = {0,0};
	SMALL_RECT writeArea = {0,0,iWidth,iHeight};

	int x,y;
	int Color;
	x=Centro.x;
	y=Centro.y;

	//Puyo en pixeles cada puyo es de 9 pxls
	//      + + +              //
	//      + + +              //
	//      + + +              //
	//Esta rutina manda el puyo al buffer de la pantalla dependiendo de su centro.
	if (estado == ACTIVO){
		switch (tipo){
			case 3: 
				Color = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | BACKGROUND_BLUE |
					BACKGROUND_INTENSITY;
				break;
			case 4:
				Color = BACKGROUND_GREEN |
					BACKGROUND_INTENSITY;
				break;
			case 5:
				Color = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | BACKGROUND_RED |
					BACKGROUND_INTENSITY;
				break;
			case 6:
				Color = FOREGROUND_BLUE | FOREGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE |
					BACKGROUND_INTENSITY;
				break;
		};

			Pantalla[x+(iWidth*y)].Char.AsciiChar = tipo; //CentroCentro
			Pantalla[(x+1)+(iWidth*y)].Char.AsciiChar = 0xBA;//CentroDer
			Pantalla[(x-1)+(iWidth*(y))].Char.AsciiChar = 0xBA;//CentroIzq

			Pantalla[(x+1)+(iWidth*(y-1))].Char.AsciiChar = 0xBB;//ArribaDer
			Pantalla[x+(iWidth*(y-1))].Char.AsciiChar = 0xCD;//ArribaCentro
			Pantalla[(x-1)+(iWidth*(y-1))].Char.AsciiChar = 0xC9;//ArribaIZq
			
			Pantalla[x+(iWidth*(y+1))].Char.AsciiChar = 0xCD;//AbajoCentro
			Pantalla[(x+1)+(iWidth*(y+1))].Char.AsciiChar = 0xBC;//AbajoDer
			Pantalla[(x-1)+(iWidth*(y+1))].Char.AsciiChar = 0xC8;//AbajoIzq

			//Add some color
			Pantalla[x+(iWidth*y)].Attributes = Color;
			Pantalla[(x+1)+(iWidth*y)].Attributes = Color;
			Pantalla[(x-1)+(iWidth*y)].Attributes = Color;
			Pantalla[x+(iWidth*(y+1))].Attributes = Color;
			Pantalla[(x+1)+(iWidth*(y+1))].Attributes = Color;
			Pantalla[(x-1)+(iWidth*(y+1))].Attributes = Color;
			Pantalla[x+(iWidth*(y-1))].Attributes = Color;
			Pantalla[(x+1)+(iWidth*(y-1))].Attributes = Color;
			Pantalla[(x-1)+(iWidth*(y-1))].Attributes = Color;
	}else{
		Pantalla[x+(iWidth*y)].Char.AsciiChar = ' ';
		Pantalla[(x+1)+(iWidth*y)].Char.AsciiChar = ' ';
		Pantalla[(x-1)+(iWidth*y)].Char.AsciiChar = ' ';

		Pantalla[(x+1)+(iWidth*(y-1))].Char.AsciiChar = ' ';
		Pantalla[x+(iWidth*(y-1))].Char.AsciiChar = ' ';
		Pantalla[(x-1)+(iWidth*(y-1))].Char.AsciiChar = ' ';

		Pantalla[(x+1)+(iWidth*(y+1))].Char.AsciiChar = ' ';
		Pantalla[x+(iWidth*(y+1))].Char.AsciiChar = ' ';
		Pantalla[(x-1)+(iWidth*(y+1))].Char.AsciiChar = ' ';
		//Add some color
		Pantalla[x+(iWidth*y)].Attributes = BACKGROUND_INTENSITY;
		Pantalla[(x+1)+(iWidth*y)].Attributes = BACKGROUND_INTENSITY;
		Pantalla[(x-1)+(iWidth*y)].Attributes = BACKGROUND_INTENSITY;
		Pantalla[x+(iWidth*(y+1))].Attributes = BACKGROUND_INTENSITY;
		Pantalla[(x+1)+(iWidth*(y+1))].Attributes = BACKGROUND_INTENSITY;
		Pantalla[(x-1)+(iWidth*(y+1))].Attributes = BACKGROUND_INTENSITY;
		Pantalla[x+(iWidth*(y-1))].Attributes = BACKGROUND_INTENSITY;
		Pantalla[(x+1)+(iWidth*(y-1))].Attributes = BACKGROUND_INTENSITY;
		Pantalla[(x-1)+(iWidth*(y-1))].Attributes = BACKGROUND_INTENSITY;
		
	}
}
bool puyo::operator== (const puyo &_puyo){
	return (estado == ACTIVO ) && (_puyo.estado == ACTIVO) && (tipo == _puyo.tipo);
}
Posicion puyo::DarCentro(){
	return Centro;
}
#include <stdlib.h>
#include <stdio.h>
#include <Windows.h>
#include <Tchar.h>
#include <vector>
#include <time.h>
#include <string>
#include <istream>
//#include <gl/glut.h>

#ifndef H_PUYO_HEADER

#define H_PUYO_HEADER
#define iWidth 60 
#define iHeight 36 // el numero de renglones 12 por el alto en pixeles 3
#define iRenglones 12 //Puyos en lo alto
#define iPilas 6  //puyos a lo ancho
#define iColumnas 18 //ancho de la pantalla de juego en pixeles; numero de pilas 6 por el tama�o en pixeles del puyo 



struct Posicion{
		int x;
		int y;
		bool operator== (const Posicion& _pos){
			return (x == _pos.x && y == _pos.y);
		};
		void operator= (const Posicion& _pos){
			x = _pos.x;
			y = _pos.y;
		};
};
enum Tipo {//los valores representan el codigo Ascii de estos caracteres
	PICA = 3,
	TREBOL = 4,
	AS = 5,
	DIAMANTE = 6
};
enum Estado {
	ACTIVO,  //activo quiere decir que el puyo esta en juego
	INACTIVO //inactivo es que no esta en juego
};
public class puyo{
public:
	CHAR_INFO Simbolo;
	Posicion Centro;
	enum Estado estado;
	enum Tipo tipo;
	bool Pivote; //  en verdadero significa que es pivote en rotaci�n esto quiere decir que no gira. 
	bool Cayendo;
	void asignarPos(int _x, int _y);
	int  obtenerCentro();
	int obtenerX();
	int obtenerY();
	int obtenerXv();
	int obtenerYv();
	void imprimePuyo(HANDLE &wHnd,CHAR_INFO *Pantalla);
	void nuevoPuyo(Posicion _Centro);
	puyo(Posicion Centro);
	~puyo();
	bool operator == (const puyo &_puyo);
	Posicion DarCentro();
};
#endif
#include <process.h>
#include <Tchar.h>
#include "..\Headers\puyo.h"

#ifndef H_TABLERO_HEADER
#define H_TABLERO_HEADER
public class Tablero{
public:
	
	Tablero();
	~Tablero();

	CHAR_INFO Pantalla[iWidth * iHeight];
	HANDLE wHnd;    // Handle escribe a la consola.
	HANDLE rHnd;    // Handle lee de la consola.

	int Score;
	int iActual[2];
	bool bJugando;
	bool Pausa;
	bool Rotar;
	bool MvDer;
	bool MvIzq;
	bool MvAbJ;
	std::vector<puyo> Puyos;

	void iniciarPantalla();
	void imprimePantalla();
	void HacerRenza(int Indice);
	void CalcularScore(int);
	void MoverAbajo(int Indice);
	void MoverDerecha(int Indice,int Act);
	void MoverIzquierda(int Indice,int Act);
	void fRotar();
	void CapturarEventos();
	bool Mover();
	void Descender();
	bool ChercarDescenso(int indice);
	void Jugando(bool Start);
	void LimpiarTablero();
	void NuevoPar();
	void PasarPuyo(int Org, int Dst);
	void MoverSuperiores(int Indice);
	void PantallaScore();
	void ImprimeGameOver();

};//end class
#endif